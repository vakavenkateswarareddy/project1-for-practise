const arrayOfObjects=require('./p1')

// problem 1
let newArray=arrayOfObjects.filter((object)=>{
    if (object["job"].match(/Web Developer/)){
        return object
    }
});
console.log(newArray);

// problem 2
let newArray1=arrayOfObjects.map((object)=>{
    let number=object["salary"].replace("$","");
    return Number(number);

});
console.log(newArray1);

// problem 3
let newArray2=arrayOfObjects.map((object)=>{
    let number=(object["salary"].replace("$",""))*10000;
    object["corrected_salary"]="$"+Number(number);
    return object;
});
console.log(newArray2);


// problem 4
let intialValue=0;
let newArray3=arrayOfObjects.reduce((previousValue, currentValue)=>{
    let number=Number(currentValue["salary"].replace("$",""))
    return previousValue+number
},intialValue);

console.log(newArray3)

// problem 5
let newArray4=arrayOfObjects.reduce((previousValue, currentValue)=>{
    if(!previousValue[currentValue['location']]){
        previousValue[currentValue['location']] = Number(currentValue["salary"].replace("$",""));
    } else {
        previousValue[currentValue['location']] += Number(currentValue["salary"].replace("$",""));
    }
    return previousValue;
}, {});

console.log(newArray4);

